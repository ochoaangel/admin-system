/* eslint-disable prefer-const */
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  constructor() {}

  set(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  get(key: string) {
    return localStorage.getItem(key) || null;
  }

  setJson(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  getJson(key: string) {
    let value = this.get(key) || null;
    if (value) {
      return JSON.parse(value);
    } else {
      return null;
    }
  }

  deleteKey(key: string) {
    localStorage.removeItem(key);
  }

  getToken() {
    const all = this.getJson('appData');
    if (all && all.hasOwnProperty('token')) {
      return all.token;
    } else {
      return null;
    }
  }

  getMe() {
    const all = this.getJson('appData');
    if (all && all.appMe) {
      return all.appMe;
    } else {
      return null;
    }
  }

  setMe(authMe: any) {
    let appData = this.getJson('appData');
    appData.appMe = authMe;
    this.setJson('appData', appData);
  }
}
