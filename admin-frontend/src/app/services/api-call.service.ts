import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiCallService {
  baseUrl;

  constructor(private http: HttpClient) {
    this.baseUrl = `https://localhost:1234`;
  }

  // authMe(): Observable<IAuthMeRes> {
  //   const url = '/api/auth/me';
  //   const urlFinal = this.baseUrl + url;
  //   return this.http.get<IAuthMeRes>(urlFinal);
  // }

  // login(params:ILoginData): Observable<ILoginRes> {
  //   const url = '/api/auth/login';
  //   const urlFinal = this.baseUrl + url;
  //   return this.http.post<ILoginRes>(urlFinal, params);
  // }
}
