import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UnauthorizerPageRoutingModule } from './unauthorizer-routing.module';

import { UnauthorizerPage } from './unauthorizer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UnauthorizerPageRoutingModule
  ],
  declarations: [UnauthorizerPage]
})
export class UnauthorizerPageModule {}
