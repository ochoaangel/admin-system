import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-unauthorizer',
  templateUrl: './unauthorizer.page.html',
  styleUrls: ['./unauthorizer.page.scss'],
})
export class UnauthorizerPage implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}

  gotoLogin() {
    this.router.navigate(['/login']);
  }
}
