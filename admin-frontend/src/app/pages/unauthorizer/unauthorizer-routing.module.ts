import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnauthorizerPage } from './unauthorizer.page';

const routes: Routes = [
  {
    path: '',
    component: UnauthorizerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnauthorizerPageRoutingModule {}
