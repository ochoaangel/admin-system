import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  role = 'admin';
  // role = 'superadmin';
  // role = 'usuario';
  constructor(private router: Router) {}

  ngOnInit() {}

  goto(url) {
    this.router.navigate([url]);
  }
}
