import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { ApiCallService } from '../../services/api-call.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  form: FormGroup;
  constructor(
    private apiCall: ApiCallService,
    private myStorage: LocalStorageService,
    private readonly fb: FormBuilder,
    private router: Router
  ) {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.myStorage.deleteKey('appData');
  }

  login() {
    // if (this.form.valid) {
    // this.apiCall.login(this.form.value).subscribe( response => {
    // this.myStorage.setJson('appData', {token: response.access_token})
    // this.apiCall.authMe().subscribe( authMe => {
    //   this.myStorage.setMe(authMe)
    //   this.router.navigate(["/"])
    // })
    // },
    // error=>{
    //   swal.fire({
    //     title: 'Invalid data',
    //     icon: 'error',
    //     confirmButtonColor: '#00ced1',
    //   })
    //   this.myStorage.deleteKey('appData')
    // })
  }
}
