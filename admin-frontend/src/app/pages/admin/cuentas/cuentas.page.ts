import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/interfaces/interfaces';
import swal from 'sweetalert2';
@Component({
  selector: 'app-cuentas',
  templateUrl: './cuentas.page.html',
  styleUrls: ['./cuentas.page.scss'],
})
export class CuentasPage implements OnInit {
  data: [];
  // dataFiltered: Usuario[];
  dataFiltered: [{ x: '2' }, { x: '4' }];
  // dataFiltered: [];
  filterText = '';

  constructor(private route: Router) {
    // this.dataFiltered = this.data;
    // this.dataFiltered = [{ x: '2' }];
    // this.dataFiltered = [];
  }

  ngOnInit() {
    // buscar tabla en Backend
    this.dataFiltered = [{ x: '2' }, { x: '4' }];
  }

  edit(id) {
    this.route.navigate(['cuentas', id]);
  }

  delete(id) {
    swal
      .fire({
        title: 'Eliminar',
        text: '¿Desea eliminarlo?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Si, borrar!',
        cancelButtonText: 'No',
        cancelButtonColor: '#d33',
      })
      .then((result) => {
        if (result.isConfirmed) {
          // this.data.filter((x) => x.id !== id);
        }
      });
  }

  textChange($event) {
    const text = $event.detail.value.toLowerCase().trim() || '';
    switch (text) {
      case '':
        break;
      default:
        // this.dataFiltered = this.data.filter((x) =>
        //   x.nombre.toLowerCase().includes(text)
        // );
        break;
    }
  }

  add() {
    this.route.navigate(['cuentas', 0]);
  }
}
