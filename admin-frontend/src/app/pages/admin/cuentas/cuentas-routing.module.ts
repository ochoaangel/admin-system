import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CuentasShowComponent } from './cuentas-show/cuentas-show.component';

import { CuentasPage } from './cuentas.page';

const routes: Routes = [
  {
    path: '',
    component: CuentasPage,
  },
  {
    path: ':id',
    component: CuentasShowComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CuentasPageRoutingModule {}
