import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cuentas-show',
  templateUrl: './cuentas-show.component.html',
  styleUrls: ['./cuentas-show.component.scss'],
})
export class CuentasShowComponent implements OnInit, OnDestroy {
  editCliente = true;
  idCliente: number;
  rolNow = 'superadmin';
  nivelIngles = ['A1', 'A2', 'B1', 'B2', 'C1', 'C2'];

  data = {
    nombreCuenta: '',
    nombreCliente: '',
    nombreResponsable: '',
  };

  customAlertOptions: any = {
    header: 'Inglés',
    subHeader: 'Seleccione el nivel de inglés',
    translucent: true,
  };

  private sub: any;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.idCliente = +params.id || 0;
      console.log(this.idCliente);
      if (this.idCliente === 0) {
        this.editCliente = false;
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  eliminar() {
    console.log('this.nivelSelected :>> ', this.data);
  }

  guardar() {
    console.log('this.nivelSelected :>> ', this.data);
  }
}
