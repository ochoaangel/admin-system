import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CuentasPageRoutingModule } from './cuentas-routing.module';

import { CuentasPage } from './cuentas.page';
import { CuentasShowComponent } from './cuentas-show/cuentas-show.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CuentasPageRoutingModule
  ],
  declarations: [CuentasPage, CuentasShowComponent]
})
export class CuentasPageModule {}
