import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movimientos-show',
  templateUrl: './movimientos-show.component.html',
  styleUrls: ['./movimientos-show.component.scss'],
})
export class MovimientosShowComponent implements OnInit , OnDestroy {
  editEquipo = true;
  idEquipo: number;
  rolNow = 'superadmin';
  nivelIngles = ['A1', 'A2', 'B1', 'B2', 'C1', 'C2'];

  data = {
    nombre: '',
  };

  private sub: any;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.idEquipo = +params.id || 0;
      console.log(this.idEquipo);
      if (this.idEquipo === 0) {
        this.editEquipo = false;
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  eliminar() {
    console.log('this.nivelSelected :>> ', this.data);
  }

  guardar() {
    console.log('this.nivelSelected :>> ', this.data);
  }
}
