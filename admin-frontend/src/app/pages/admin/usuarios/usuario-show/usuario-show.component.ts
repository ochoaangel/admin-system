import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-show',
  templateUrl: './usuario-show.component.html',
  styleUrls: ['./usuario-show.component.scss'],
})
export class UsuarioShowComponent implements OnInit, OnDestroy {
  editUsuario = true;
  idUsuario: number;
  rolNow = 'superadmin';
  nivelIngles = ['A1', 'A2', 'B1', 'B2', 'C1', 'C2'];

  data = {
    nivelSelected: '',
    status: true,
    admin: false,
    nombre: '',
    correo: '',
    link: '',
    conocimiento: '',
  };

  customAlertOptions: any = {
    header: 'Inglés',
    subHeader: 'Seleccione el nivel de inglés',
    translucent: true,
  };

  private sub: any;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.idUsuario = +params.id || 0;
      console.log(this.idUsuario);
      if (this.idUsuario === 0) {
        this.editUsuario = false;
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  eliminar() {
    console.log('this.nivelSelected :>> ', this.data);
  }

  guardar() {
    console.log('this.nivelSelected :>> ', this.data);
  }
}
