import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EquiposPageRoutingModule } from './equipos-routing.module';

import { EquiposPage } from './equipos.page';
import { EquiposShowComponent } from './equipos-show/equipos-show.component';
// import { HeaderComponent } from '../../home/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EquiposPageRoutingModule
  ],
  declarations: [EquiposPage, EquiposShowComponent]
})
export class EquiposPageModule {}
