import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EquiposShowComponent } from './equipos-show/equipos-show.component';

import { EquiposPage } from './equipos.page';

const routes: Routes = [
  {
    path: '',
    component: EquiposPage,
  },
  {
    path: ':id',
    component: EquiposShowComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EquiposPageRoutingModule {}
