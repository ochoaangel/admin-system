import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { UserGuard } from './guard/user.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'unauthorizer',
    loadChildren: () =>
      import('./pages/unauthorizer/unauthorizer.module').then(
        (m) => m.UnauthorizerPageModule
      ),
  },
  {
    path: '',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomePageModule),
    canActivate: [UserGuard],
  },
  {
    path: 'usuarios',
    loadChildren: () =>
      import('./pages/admin/usuarios/usuarios.module').then(
        (m) => m.UsuariosPageModule
      ),
    canActivate: [UserGuard],
  },
  {
    path: 'cuentas',
    loadChildren: () =>
      import('./pages/admin/cuentas/cuentas.module').then(
        (m) => m.CuentasPageModule
      ),
    canActivate: [UserGuard],
  },
  {
    path: 'equipos',
    loadChildren: () =>
      import('./pages/admin/equipos/equipos.module').then(
        (m) => m.EquiposPageModule
      ),
    canActivate: [UserGuard],
  },
  {
    path: 'movimientos',
    loadChildren: () =>
      import('./pages/admin/movimientos/movimientos.module').then(
        (m) => m.MovimientosPageModule
      ),
    canActivate: [UserGuard],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
