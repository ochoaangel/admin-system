export interface Usuario {
  id?: number;
  nombre?: string;
  rol?: string;
  correo?: string;
}
