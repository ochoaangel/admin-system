/* eslint-disable prefer-const */
import { PrismaClient } from '@prisma/client';
import { name, company, internet } from 'faker';
// import faker from 'faker/dist/faker.js';

const prisma = new PrismaClient();

function randomIntFromInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

async function main() {
  // delete all users
  await prisma.usuariosEnEquipos.deleteMany();
  await prisma.equipo.deleteMany();
  await prisma.usuario.deleteMany();
  await prisma.cuentaCliente.deleteMany();
  // await prisma.movimiento.deleteMany();

  // add users
  await prisma.usuario.createMany({
    data: [
      {
        nombre: 'superadmin',
        rol: 'superadmin',
        conocimientos: 'Fullstack',
        contrasena: 'superadmin',
        correo: 'superadminFullstack@gmail.com',
        ingles: 'C2',
        linkcv: internet.domainName(),
      },
      {
        nombre: 'admin',
        rol: 'admin',
        conocimientos: 'Backend',
        contrasena: 'admin',
        correo: 'adminBackend@gmail.com',
        ingles: 'C2',
        linkcv: internet.domainName(),
      },
    ],
  });
  /////////////////////////////////////////////////////////////////////////////////
  // bucle para crear equipos
  for (let index = 0; index < randomIntFromInterval(5, 30); index++) {
    let equipoFaked = [];
    // bucle para crear usuarios dentro de un equipo
    for (let i = 0; i < randomIntFromInterval(1, 5); i++) {
      let usuariosFaked = [];
      let nivelIngles = ['A', 'B', 'C'];
      for (let n = 0; n < randomIntFromInterval(2, 10); n++) {
        let myname = `${name.firstName()}`;
        let mytemp = `${name.jobType()}`;
        usuariosFaked.push({
          usuario: {
            create: {
              nombre: myname,
              conocimientos: mytemp,
              contrasena: myname,
              correo: `${myname + mytemp}@gmail.com`,
              linkcv: internet.domainName(),
              ingles: `${
                nivelIngles[randomIntFromInterval(0, 2)] +
                randomIntFromInterval(1, 2)
              }`,
            },
          },
        });
      }
      equipoFaked.push({
        nombreEquipo: name.jobArea() + randomIntFromInterval(2, 2000),
        usuarios: {
          create: usuariosFaked,
        },
      });
    }

    await prisma.cuentaCliente.create({
      data: {
        nombreCuenta:
          company
            .companyName()
            .replace(/\s+/g, '')
            .replace(/,+/g, '')
            .replace(/-+/g, '') + randomIntFromInterval(2, 10),
        nombreCliente: `${name.firstName()} ${name.lastName()}`,
        nombreResponsable: `${name.firstName()} ${name.lastName()}`,
        equipo: {
          create: equipoFaked,
        },
      },
    });
  }

  ////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////

  // create ClienteGoogle
  //   await prisma.cuentaCliente.create({
  //     data: {
  //       nombreCuenta: 'Google',
  //       nombreCliente: 'Google',
  //       nombreResponsable: 'Martin Guerrero',
  //       equipo: {
  //         create: [
  //           {
  //             nombreEquipo: 'GoogleEquipo1',
  //             usuarios: {
  //               create: [
  //                 {
  //                   usuario: {
  //                     create: {
  //                       nombre: 'María',
  //                       conocimientos: 'FrontEnd',
  //                       contrasena: 'María',
  //                       correo: 'mariafrontend@gmail.com',
  //                       ingles: 'a2',
  //                     },
  //                   },
  //                 },
  //                 {
  //                   usuario: {
  //                     create: {
  //                       nombre: 'María2',
  //                       conocimientos: 'FrontEnd2',
  //                       contrasena: 'María',
  //                       correo: 'maria2frontend@gmail.com',
  //                       ingles: 'a2',
  //                     },
  //                   },
  //                 },
  //               ],
  //             },
  //           },
  //         ],
  //       },
  //     },
  //   });
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
